# CRUD - NodeJS

### Ligando o servidor (Linux: use sudo)
```sh
    npm install ou npm i
    npm start
```

### Testando com testes unitários (Linux: use sudo)
```sh
    npm test
```

### Testando com cURL
- Create
```sh
curl -X POST http://localhost:3000/create/boys -d '{"name": "Lucas Bittencourt", "nickname": "Naja", "password": "123"}' -H "content-type: application/json"
```

- Read (browser)
    - ```http://127.0.0.1:3000/select/boys```
    - ```http://127.0.0.1:3000/select/boys/1```

- Update
```sh
curl -X PUT http://localhost:3000/update/boys -d '{"_id": "1", "name": "Lucao", "nickname": "luquinhas", "password": "321"}' -H "content-type: application/json"
```

- Delete
```sh
curl -X DELETE http://localhost:3000/delete/boys -d '{"_id": "1"}' -H "content-type: application/json"
```