const _USER = 'root'
const _PASSWORD = ''
const _DATABASE = 'nodejs'
const _PORT = process.env.PORT || 3000

module.exports = {
	_USER,
	_PASSWORD,
	_DATABASE,
	_PORT
}
