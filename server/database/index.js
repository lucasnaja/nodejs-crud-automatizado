const mysql = require('promise-mysql')
const { _USER: user, _PASSWORD: password, _DATABASE: database } = require('../configs/global')

class Database {
	async connect() {
		this.conn = await mysql.createConnection({ user, password, database })
	}
}

module.exports = new Database()
