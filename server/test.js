const axios = require('axios')
const assert = require('assert')
const { _PORT: port } = require('./configs/global')
const request = axios.default.create({ baseURL: `http://localhost:${port}` })
const _TABLE = 'boys'

describe('CRUD no Banco de Dados', () => {
	it(`POST - create/${_TABLE}`, async () => {
		const response = await request.post(`/create/${_TABLE}`, {
			name: 'Lucas Bittencourt',
			nickname: 'Naja',
			password: '123'
		})

		assert.deepEqual(response.status, '200')
	})

	it(`GET - select/${_TABLE}`, async () => {
		const response = await request.get(`/select/${_TABLE}`)

		assert.deepEqual(response.status, '200')
	})

	it(`GET - select/${_TABLE}/1`, async () => {
		const response = await request.get(`/select/${_TABLE}`, { params: '1' })

		assert.deepEqual(response.status, '200')
	})

	it(`PUT - update/${_TABLE}`, async () => {
		const response = await request.put(`/update/${_TABLE}`, {
			_id: '1',
			name: 'Lucão',
			nickname: 'Luquinhas',
			password: '321'
		})

		assert.deepEqual(response.status, '200')
	})

	it(`DELETE - delete/${_TABLE}`, async () => {
		const response = await request.delete(`/delete/${_TABLE}`, { data: { _id: '1' } })

		assert.deepEqual(response.status, '200')
	})
})
